namespace CyberpunkRed.Vitality
{
    using System;
    using CyberpunkRed.Io.Dto;

    public class CharacterHealth
    {
        public int Hitpoints { get; private set; }

        private int MaxHitpoints;

        private CharacterHealth() { }

        public static CharacterHealth InitialiseCharacterHealthWithHitpoints(int hitpoints)
        {
            return new CharacterHealth
            {
                Hitpoints = hitpoints,
                MaxHitpoints = hitpoints,
            };
        }

        public static CharacterHealth CreateWithDto(CharacterHealthDto healthDto)
        {
            return new CharacterHealth
            {
                Hitpoints = healthDto.Hitpoints,
                MaxHitpoints = healthDto.MaxHitpoints,
            };
        }

        public CharacterHealthDto GetDto()
        {
            return new CharacterHealthDto
            {
                Hitpoints = Hitpoints,
                MaxHitpoints = MaxHitpoints,
            };
        }

        public void Injure(int damagePoints)
        {
            if (IsDamagePointsValid(damagePoints))
            {
                damagePoints = damagePoints * -1;

                ChangeHitpoints(damagePoints);
            }
            else
            {
                throw new InvalidOperationException("Cannot take negative damage!");
            }
        }
    
        private bool IsDamagePointsValid(int damagePoints)
        {
            if (damagePoints >= 0)
            {
                return true;
            }

            return false;
        }

        public void Heal(int healingPoints)
        {
            if (IsHealingPointsValid(healingPoints))
            {
                ChangeHitpoints(healingPoints);
            }
            else
            {
                throw new InvalidOperationException("Cannot heal negative!");
            }
        }

        private void ChangeHitpoints(int changeBy)
        {
            Hitpoints += changeBy;

            if (Hitpoints > MaxHitpoints)
            {
                Hitpoints = MaxHitpoints;
            }

            if (Hitpoints < 0)
            {
                Hitpoints = 0;
            }
        }

        private bool IsHealingPointsValid(int healingPoints)
        {
            if (healingPoints >= 0)
            {
                return true;
            }

            return false;
        }
    }
}