namespace CyberpunkRed.Vitality
{
    using System;
    using CyberpunkRed.Io.Dto;

    public class CharacterHumanity
    {
        public int Humanity { get; private set; }

        public int MaxHumanity { get; private set; }

        private CharacterHumanity() { }

        public static CharacterHumanity CreateHumanityWithValue(int humanity)
        {
            return new CharacterHumanity
            {
                Humanity = humanity,
                MaxHumanity = humanity,
            };
        }

        public static CharacterHumanity CreateHumanityWithDto(CharacterHumanityDto humanityDto)
        {
            return new CharacterHumanity
            {
                Humanity = humanityDto.Humanity,
                MaxHumanity = humanityDto.MaxHumanity,
            };
        }

        public CharacterHumanityDto GetDto()
        {
            return new CharacterHumanityDto
            {
                Humanity = Humanity,
                MaxHumanity = MaxHumanity,
            };
        }

        public void LoseHumanity(int humanityCost)
        {
            if (IsHumanityCostValid(humanityCost))
            {
                Humanity -= humanityCost;

                if (Humanity < 0)
                {
                    Humanity = 0;
                }
            }
            else
            {
                throw new InvalidOperationException("Cannot heal negative!");
            }
        }

        private bool IsHumanityCostValid(int humanityCost)
        {
            if (humanityCost >= 0)
            {
                return true;
            }

            return false;
        }

        public void RestoreHumanity(int humanityRestorationPoints)
        {
            if (IsHumanityRestorationPointsValid(humanityRestorationPoints))
            {
                Humanity += humanityRestorationPoints;

                if (Humanity > MaxHumanity)
                {
                    Humanity = MaxHumanity;
                }
            }
            else
            {
                throw new InvalidOperationException("Cannot heal negative!");
            }
        }

        private bool IsHumanityRestorationPointsValid(int humanityRestorationPoints)
        {
            if (humanityRestorationPoints >= 0)
            {
                return true;
            }

            return false;
        }
    }
}