﻿namespace CyberpunkRed
{
    using System;
    using CyberpunkRed.Stat;
    using CyberpunkRed.Vitality;
    using CyberpunkRed.Io;
    using CyberpunkRed.Io.Dto;

    public class Character
    {
        public string Handle { get; private set; }

        public CharacterHealth Health { get; set; }

        public CharacterHumanity Humanity  { get; private set; }

        public CharacterStatCollection Stats { get; private set; }

        private Character()
        {
        }

        public static Character CreateBasicCharacterSheet(string handle)
        {
            var sheet = new Character
            {
                Handle = handle,
                Stats = CharacterStatCollection.CreateWithStandardStats(),
            };

            return sheet;
        }

        public static Character CreateCharacterFromDto(CharacterDto characterDto)
        {
            return new Character
            {
                Handle = characterDto.Handle,
                Health = CharacterHealth.CreateWithDto(characterDto.Health),
                Humanity = CharacterHumanity.CreateHumanityWithDto(characterDto.Humanity),
                Stats = CharacterStatCollection.CreateFromDto(characterDto.Stats),
            };
        }

        public CharacterDto GetDto()
        {
            return new CharacterDto
            {
                Handle = Handle,
                Health = Health.GetDto(),
                Humanity = Humanity.GetDto(),
                Stats = Stats.GetDto(),
            };
        }

        public void DeriveHealthAndHumanity()
        {
            if (Stats.IsAllStatsInitialised())
            {
                DeriveHealth();
                DeriveHumanity();
            }
            else
            {
                throw new InvalidOperationException("All stats must have values first!");
            }
        }

        private void DeriveHumanity()
        {
            int empathyValue = Stats.GetStatByName("Empathy").Value;

            int humanityValue = empathyValue * 10;

            Humanity = CharacterHumanity.CreateHumanityWithValue(humanityValue);
        }

        private void DeriveHealth()
        {
            int willValue = Stats.GetStatByName("Willpower").Value;
            int bodyValue = Stats.GetStatByName("Body").Value;

            int averageRoundedUp = AverageOfValuesRoundedUp(willValue, bodyValue);
            int hitPoints = 10 + (5 * averageRoundedUp);
            
            Health = CharacterHealth.InitialiseCharacterHealthWithHitpoints(hitPoints);
        }

        private int AverageOfValuesRoundedUp(int value1, int value2)
        {
            decimal average = (value1 + value2) / 2;

            int roundedUp = (int)Math.Ceiling(average);

            return roundedUp;
        }
    }
}
