namespace CyberpunkRed.Stat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CyberpunkRed.Io.Dto;
    using static CyberpunkRed.Stat.CharacterStatConstants;

    public class CharacterStatCollection
    {
        private Dictionary<string, CharacterStat> _stats;

        private CharacterStatCollection() { }

        private static readonly string[] _variableStatNames = new[]
        {
            "Luck",
        };

        private CharacterStatCollection(IEnumerable<CharacterStat> stats)
        {
            InitialiseDictionaryWithStats(stats);
        }

        public static CharacterStatCollection CreateWithStandardStats()
        {
            var stats = CharacterStatConstants.StandardStatNamesAndGroups.Select(CreateCharacterStatFromTuple);

            return new CharacterStatCollection(stats);
        }

        public static CharacterStatCollection CreateFromDto(CharacterStatCollectionDto statCollectionDto)
        {
            var stats = statCollectionDto.Stats.Select(CreateCharacterStateFromDto);

            return new CharacterStatCollection(stats);
        }

        public CharacterStatCollectionDto GetDto()
        {
            return new CharacterStatCollectionDto
            {
                Stats = _stats.Select(s => s.Value.GetCharacterStatDto()).ToList(),
            };
        }

        private static CharacterStat CreateCharacterStateFromDto(CharacterStatDto statDto)
        {
            if (_variableStatNames.Contains(statDto.Name))
            {
                return new VariableCharacterStat(statDto);
            }
            else
            {
                return new CharacterStat(statDto);
            }
        }

        public static CharacterStat CreateCharacterStatFromTuple((string name, StatGroup statGroup) statDetails)
        {
            if (_variableStatNames.Contains(statDetails.name))
            {
                return new VariableCharacterStat(statDetails.name, statDetails.statGroup);
            }
            else
            {
                return new CharacterStat(statDetails.name, statDetails.statGroup);
            }
        }

        private void InitialiseDictionaryWithStats(IEnumerable<CharacterStat> stats)
        {
            var statAndNamePairs = stats.Select(s => KeyValuePair.Create(s.Name, s));

            _stats = new Dictionary<string, CharacterStat>(statAndNamePairs);
        }

        public CharacterStat GetStatByName(string name)
        {
            if (_stats.ContainsKey(name))
            {
                return _stats[name];
            }
            else
            {
                throw new InvalidOperationException($"Stat with name {name} not present.");
            }
        }

        public IEnumerable<CharacterStat> GetStatsByGroup(StatGroup group)
        {
            var statsMatchingGroup = _stats.Where(s => s.Value.Group == group);

            var statsWithoutKey = statsMatchingGroup.Select(s => s.Value);

            return statsWithoutKey;
        }

        public void SetStatInitialValue(string statName, int initialValue)
        {
            var stat = GetStatByName(statName);

            stat.SetInitialValue(initialValue);

            if (stat is VariableCharacterStat variableStat)
            {
                variableStat.InitialiseVariableValue();
            }
        }

        public bool IsAllStatsInitialised()
        {
            foreach(var stat in _stats)
            {
                if (!stat.Value.IsValueInitialised())
                {
                    return false;
                }
            }

            return true;
        }
    }
}