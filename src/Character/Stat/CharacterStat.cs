namespace CyberpunkRed.Stat
{
    using System;
    using CyberpunkRed.Io.Dto;
    using static CyberpunkRed.Stat.CharacterStatConstants;

    public class CharacterStat
    {
        public string Name { get; private set; }

        public StatGroup Group { get; private set; }

        public int Value { get; private set; }

        public CharacterStat(string name, StatGroup group) =>
            (Name, Group, Value) = (name, group, -1);

        public CharacterStat(CharacterStatDto statDto)
        {
            Name = statDto.Name;
            Group = statDto.StatGroup;
            Value = statDto.Value;
        }

        public virtual CharacterStatDto GetCharacterStatDto()
        {
            return new CharacterStatDto
            {
                Name = Name,
                StatGroup = Group,
                Value = Value,
                VariableValue = -1,
            };
        }

        public void SetInitialValue(int value)
        {
            if (CanSetValue(value))
            {
                Value = value;
            }
            else
            {
                throw new InvalidOperationException("Value already set or invalid value.");
            }
        }

        private bool CanSetValue(int value)
        {
            if (IsValueInitialised())
            {
                return false;
            }

            if (!IsValidValue(value))
            {
                return false;
            }

            return true;
        }

        private bool IsValidValue(int value)
        {
            if (value > 8)
            {
                return false;
            }

            if (value < 1)
            {
                return false;
            }

            return true;
        }

        public virtual bool IsValueInitialised()
        {
            return Value != -1;
        }
    }
}