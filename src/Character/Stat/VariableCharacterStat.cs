namespace CyberpunkRed.Stat
{
    using System;
    using CyberpunkRed.Io.Dto;
    using static CyberpunkRed.Stat.CharacterStatConstants;

    public class VariableCharacterStat : CharacterStat
    {
        public int VariableValue { get; private set; }

        public VariableCharacterStat(string name, StatGroup group) : base(name, group)
        {
            VariableValue = -1;
        }

        public VariableCharacterStat(CharacterStatDto statDto) : base(statDto)
        {
            VariableValue = statDto.VariableValue;
        }

        public override CharacterStatDto GetCharacterStatDto()
        {
            return new CharacterStatDto
            {
                Name = Name,
                StatGroup = Group,
                Value = Value,
                VariableValue = VariableValue,
            };
        }

        public void InitialiseVariableValue()
        {
            if (!IsVariableValueInitialised())
            {
                VariableValue = Value;
            }
            else
            {
                throw new InvalidOperationException("Value already initialised!");
            }
        }

        public override bool IsValueInitialised()
        {
            if (Value == -1)
            {
                return false;
            }

            if (VariableValue == -1)
            {
                return false;
            }

            return true;
        }

        public void UseValue(int useBy)
        {
            if (IsUsageValid(useBy))
            {
                useBy = useBy * -1;

                ChangeVariableValue(useBy);
            }
            else
            {
                throw new InvalidOperationException("Cannot use negative value or value unitialised!");
            }
        }

        public void RestoreValue(int restoreBy)
        {
            if (IsUsageValid(restoreBy))
            {
                ChangeVariableValue(restoreBy);
            }
            else
            {
                throw new InvalidOperationException("Invalid value, or variable not initialised.");
            }
        }
    
        private bool IsUsageValid(int usage)
        {
            if (!IsVariableValueInitialised())
            {
                return false;
            }
            
            if (!IsUsageValueValid(usage))
            {
                return false;
            }

            return true;
        }

        private bool IsVariableValueInitialised()
        {
            return VariableValue != -1;
        }

        private bool IsUsageValueValid(int usage)
        {
            if (usage < 0)
            {
                return false;
            }

            if (usage > 8)
            {
                return false;
            }

            if (usage > Value)
            {
                return false;
            }

            return true;
        }

        private void ChangeVariableValue(int changeBy)
        {
            VariableValue += changeBy;

            if (VariableValue > Value)
            {
                VariableValue = Value;
            }

            if (VariableValue < 0)
            {
                VariableValue = 0;
            }
        }
    }
}