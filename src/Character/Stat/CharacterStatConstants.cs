namespace CyberpunkRed.Stat
{
    using System.Collections.Generic;

    public class CharacterStatConstants
    {
        public static IEnumerable<(string name, StatGroup statGroup)> StandardStatNamesAndGroups = new[]
        {
            ("Intelligence", StatGroup.Mental),
            ("Willpower", StatGroup.Mental),
            ("Cool", StatGroup.Mental),
            ("Empathy", StatGroup.Mental),
            ("Technique", StatGroup.Combat),
            ("Reflexes", StatGroup.Combat),
            ("Luck", StatGroup.Fortune),
            ("Body", StatGroup.Physical),
            ("Dexterity", StatGroup.Physical),
            ("Movement", StatGroup.Physical),
        };
        
        public enum StatGroup
        {
            Mental,
            Combat,
            Fortune,
            Physical,
        }
    }
}