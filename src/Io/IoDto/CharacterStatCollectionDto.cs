namespace CyberpunkRed.Io.Dto
{
    using System.Collections.Generic;
    
    public class CharacterStatCollectionDto
    {
        public List<CharacterStatDto> Stats { get; set; }
    }
}