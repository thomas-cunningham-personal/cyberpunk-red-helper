namespace CyberpunkRed.Io.Dto
{
    public class CharacterHumanityDto
    {
        public int Humanity { get; set; }

        public int MaxHumanity { get; set; }
    }
}