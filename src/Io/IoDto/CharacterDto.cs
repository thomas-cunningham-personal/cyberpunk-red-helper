namespace CyberpunkRed.Io.Dto
{
    public class CharacterDto
    {
        public string Handle { get; set; }

        public CharacterHealthDto Health { get; set; }

        public CharacterHumanityDto Humanity { get; set; }

        public CharacterStatCollectionDto Stats { get; set; }
    }
}