namespace CyberpunkRed.Io.Dto
{
    using static CyberpunkRed.Stat.CharacterStatConstants;

    public class CharacterStatDto
    {
        public string Name { get; set; }

        public StatGroup StatGroup { get; set; }

        public int Value { get; set; }

        public int VariableValue { get; set; }
    }
}