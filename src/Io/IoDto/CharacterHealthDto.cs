namespace CyberpunkRed.Io.Dto
{
    public class CharacterHealthDto
    {
        public int Hitpoints { get; set; }

        public int MaxHitpoints { get; set; }
    }
}