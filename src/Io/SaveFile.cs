namespace CyberpunkRed.Io
{
    using System;
    using System.IO;
    using System.Text.Json;
    using System.Threading.Tasks;
    using CyberpunkRed.Io.Dto;
    
    public class SaveFile : ICharacterDtoProvider, ICharacterDtoStorage
    {
        public async Task<CharacterDto> GetCharacterDtoAsync(string characterName)
        {
            try
            {
                return await AttemptLoad(characterName);
            }
            catch (NotSupportedException nse)
            {
                throw new Exception("Failed to serialise character dto!", nse);
            }
            catch (IOException ioe)
            {
                throw new Exception("Failed to write character to save file!", ioe);
            }
            catch (Exception e)
            {
                throw new Exception("Unidentified exception saving character.", e);
            }
        }

        private async Task<CharacterDto> AttemptLoad(string characterName)
        {
            string saveName = $"{characterName}.character";

            string serialisedCharacter = await File.ReadAllTextAsync(saveName);

            return JsonSerializer.Deserialize<CharacterDto>(serialisedCharacter);
        }

        public async Task SaveCharacterDtoAsync(CharacterDto character)
        {
            try
            {
                await AttemptSave(character);
            }
            catch (NotSupportedException nse)
            {
                throw new Exception("Failed to serialise character dto!", nse);
            }
            catch (IOException ioe)
            {
                throw new Exception("Failed to write character to save file!", ioe);
            }
            catch (Exception e)
            {
                throw new Exception("Unidentified exception saving character.", e);
            }
        }

        private async Task AttemptSave(CharacterDto character)
        {
            string serialisedCharacter = JsonSerializer.Serialize(character);

            string saveName = $"{character.Handle}.character";

            await File.WriteAllTextAsync(saveName, serialisedCharacter);
        }
    }
}