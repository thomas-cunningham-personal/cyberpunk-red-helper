namespace CyberpunkRed.Io
{
    using System.Threading.Tasks;
    using CyberpunkRed.Io.Dto;
    
    public interface ICharacterDtoStorage
    {
        Task SaveCharacterDtoAsync(CharacterDto character);
    }
}