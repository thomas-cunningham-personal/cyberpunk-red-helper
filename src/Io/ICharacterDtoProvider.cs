namespace CyberpunkRed.Io
{
    using System.Threading.Tasks;
    using CyberpunkRed.Io.Dto;
    
    public interface ICharacterDtoProvider
    {
        Task<CharacterDto> GetCharacterDtoAsync(string characterName);
    }
}