namespace CyberpunkRed.Role
{
    using System;
    using CyberpunkRed.Role.RoleAbility;

    public abstract class Role
    {
        public string GetRoleName()
        {
            return GetType().Name;
        }

        public RoleAbility.RoleAbility GetRoleAbility()
        {
            throw new NotImplementedException();
        }
    }
}