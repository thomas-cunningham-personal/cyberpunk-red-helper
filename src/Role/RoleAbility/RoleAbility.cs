namespace CyberpunkRed.Role.RoleAbility
{
    public abstract class RoleAbility
    {
        public abstract string GetRoleAbilityName();

        public abstract int GetRoleAbilityLevel();

        public abstract string GetRoleAbilityDescription();
    }
}