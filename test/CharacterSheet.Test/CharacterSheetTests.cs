namespace CyberpunkRed.Test.Character
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CyberpunkRed;

    [TestClass]
    public class CharacterTests
    {
        [TestMethod]
        public void TestBasicCreationFactory1()
        {
            var character = Character.CreateBasicCharacterSheet("Test name");

            Assert.IsNotNull(character);
        }

        [TestMethod]
        public void TestBasicCreationFactory2()
        {
            const string name = "Xen0";

            var character = Character.CreateBasicCharacterSheet(name);

            Assert.AreEqual(name, character.Handle);
        }

        [TestMethod]
        public void TestDerivedHealth1()
        {
            const int expected = 50;

            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();

            Assert.AreEqual(expected, character.Health.Hitpoints);
        }

        [TestMethod]
        public void TestDerivedHealth2()
        {
            const int expected = 20;

            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 2);
            character.DeriveHealthAndHumanity();

            Assert.AreEqual(expected, character.Health.Hitpoints);
        }

        [TestMethod]
        public void TestDerivedHumanity1()
        {
            const int expected = 10;

            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 1);
            character.DeriveHealthAndHumanity();

            Assert.AreEqual(expected, character.Humanity.Humanity);
        }

        [TestMethod]
        public void TestDerivedHumanity2()
        {
            const int expected = 80;

            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();

            Assert.AreEqual(expected, character.Humanity.Humanity);
        }

        [TestMethod]
        public void TestDamage1()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 40;

            character.Health.Injure(10);

            Assert.AreEqual(expected, character.Health.Hitpoints);
        }

        [TestMethod]
        public void TestDamage2()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 45;

            character.Health.Injure(10);
            character.Health.Heal(5);

            Assert.AreEqual(expected, character.Health.Hitpoints);
        }

        [TestMethod]
        public void TestDamage3()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 0;

            character.Health.Injure(60);

            Assert.AreEqual(expected, character.Health.Hitpoints);
        }

        [TestMethod]
        public void TestHumanity1()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 70;

            character.Humanity.LoseHumanity(10);

            Assert.AreEqual(expected, character.Humanity.Humanity);
        }

        [TestMethod]
        public void TestHumanity2()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 70;

            character.Humanity.LoseHumanity(15);
            character.Humanity.RestoreHumanity(5);

            Assert.AreEqual(expected, character.Humanity.Humanity);
        }

        [TestMethod]
        public void TestHumanity3()
        {
            var character = Character.CreateBasicCharacterSheet("Xen0");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            const int expected = 0;

            character.Humanity.LoseHumanity(100);

            Assert.AreEqual(expected, character.Humanity.Humanity);
        }

        private void SetAllCharacterStatisticsToValue(Character character, int value)
        {
            character.Stats.SetStatInitialValue("Intelligence", value);
            character.Stats.SetStatInitialValue("Willpower", value);
            character.Stats.SetStatInitialValue("Cool", value);
            character.Stats.SetStatInitialValue("Empathy", value);
            character.Stats.SetStatInitialValue("Technique", value);
            character.Stats.SetStatInitialValue("Reflexes", value);
            character.Stats.SetStatInitialValue("Luck", value);
            character.Stats.SetStatInitialValue("Body", value);
            character.Stats.SetStatInitialValue("Dexterity", value);
            character.Stats.SetStatInitialValue("Movement", value);
        }
    }
}
