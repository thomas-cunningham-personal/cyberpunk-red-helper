namespace CyberpunkRed.Test.Io
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CyberpunkRed;
    using CyberpunkRed.Io;
    using System.Threading.Tasks;

    [TestClass]
    public class IoTests
    {
        [TestMethod]
        public async Task TestSave1()
        {
            var character = Character.CreateBasicCharacterSheet("Test name");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            var save = new SaveFile();
            var dto = character.GetDto();

            await save.SaveCharacterDtoAsync(dto);
            var retreivedDto = await save.GetCharacterDtoAsync(dto.Handle);

            Assert.AreEqual(character.Handle, retreivedDto.Handle);
            Assert.AreEqual(character.Health.Hitpoints, retreivedDto.Health.Hitpoints);
            Assert.AreEqual(character.Humanity.Humanity, retreivedDto.Humanity.Humanity);
        }

        [TestMethod]
        public async Task TestSave2()
        {
            var character = Character.CreateBasicCharacterSheet("Test name");
            SetAllCharacterStatisticsToValue(character, 8);
            character.DeriveHealthAndHumanity();
            var save = new SaveFile();
            var dto = character.GetDto();
            
            await save.SaveCharacterDtoAsync(dto);
            var retreivedDto = await save.GetCharacterDtoAsync(dto.Handle);
            var retrievedCharacter = Character.CreateCharacterFromDto(retreivedDto);

            Assert.AreEqual(character.Handle, retrievedCharacter.Handle);
            Assert.AreEqual(character.Health.Hitpoints, retrievedCharacter.Health.Hitpoints);
            Assert.AreEqual(character.Humanity.Humanity, retrievedCharacter.Humanity.Humanity);
        }

        private void SetAllCharacterStatisticsToValue(Character character, int value)
        {
            character.Stats.SetStatInitialValue("Intelligence", value);
            character.Stats.SetStatInitialValue("Willpower", value);
            character.Stats.SetStatInitialValue("Cool", value);
            character.Stats.SetStatInitialValue("Empathy", value);
            character.Stats.SetStatInitialValue("Technique", value);
            character.Stats.SetStatInitialValue("Reflexes", value);
            character.Stats.SetStatInitialValue("Luck", value);
            character.Stats.SetStatInitialValue("Body", value);
            character.Stats.SetStatInitialValue("Dexterity", value);
            character.Stats.SetStatInitialValue("Movement", value);
        }
    }
}